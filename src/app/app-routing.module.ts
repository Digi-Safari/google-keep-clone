import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from '../app/login/login.component';
import { SignupComponent } from '../app/signup/signup.component';
import { DashboardComponent } from '../app/dashboard/dashboard.component';

const routes: Routes = [
  {path:'login', component : LoginComponent},
  {path:'signup', component : SignupComponent},
  {path: 'notes', component : DashboardComponent},
  {path:'', redirectTo:'/notes', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
