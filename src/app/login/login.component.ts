import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm : FormGroup;
  constructor(private formBuilder : FormBuilder) { 
    this.loginForm = this.formBuilder.group({
      email : ['',Validators.compose([Validators.required,Validators.minLength(5)])],
      password: ['',Validators.compose([Validators.required,Validators.minLength(5)])]
    })
  }

  ngOnInit(): void {
  }

  doUserLogin(lForm: FormGroup){
    console.log(lForm.value);
    
  }

}
